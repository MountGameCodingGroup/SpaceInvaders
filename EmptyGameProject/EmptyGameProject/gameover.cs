﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyGameProject
{
    class Gameover : GameObjectList
    {

        private TextGameObject title;

        public Gameover(int layer = 0, string id = "") : base(layer, id)
        {
            title = new TextGameObject("GameFont");
            title.Text = "Game Over";
            title.Position = new Vector2(GameEnvironment.Screen.X / 2 - title.Size.X / 2, GameEnvironment.Screen.Y / 2);
            Add(title);

        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            if (inputHelper.IsKeyDown(Keys.Enter) )
            {
                GameEnvironment.GameStateManager.SwitchTo("level");
            }

        }
    }
}
