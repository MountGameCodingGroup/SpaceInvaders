﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyGameProject
{
    class Level : GameObjectList
    {
        private SpriteGameObject background;
        private Player player;
        private int numberOfEnemies;
        private EnemyContainer enemies;
        private Bullet bullet;
        private int points;
        private TextGameObject score;

        public Level(int layer = 0, string id = "") : base(layer, id)
        {
            background = new SpriteGameObject("spr_background");
            Add(background);

            bullet = new Bullet("spr_bullet");
            Add(bullet);

            player = new Player("spr_ship");
            Add(player);

            numberOfEnemies = 30;
            enemies = new EnemyContainer();
            Add(enemies);

            for(int i = 0; i < numberOfEnemies; i++)
            {
                enemies.Add(new RedInvader("spr_red_invader"));
            }

            setupEnemies();
            
            points = 0;
            score = new TextGameObject("GameFont");
            score.Position = new Vector2(4, 4);
            score.Text = "" + points;
            Add(score);
        }

        public override void Reset()
        {
            base.Reset();

            points = 0;
            score.Text = "" + points;

            setupEnemies();
        }

        private void setupEnemies()
        {
            int spacingX = GameEnvironment.Screen.X / 10;
            int spacingY = 46;
            int offsetX = spacingX - 32;            
            int count = 0;
            foreach (RedInvader invader in enemies.Children)
            {
                invader.Position = new Vector2((count % 10 * spacingX) + offsetX / 2, 
                                                spacingY + (spacingY * (int)Math.Floor((double)count/10) ));
                count++;
            }
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            if (inputHelper.IsKeyDown(Keys.Space) && bullet.Visible == false)
            {
                bullet.Fire(player.Position);
            }
            
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            foreach (RedInvader invader in enemies.Children)
            {
                if (invader.CollidesWith(bullet))
                {
                    invader.HitBy(bullet);
                    bullet.HitBy(invader);
                    points++;
                    score.Text = "" + points;
                }

                if (invader.CollidesWith(player) || invader.Position.Y > player.Position.Y + player.Height || points == numberOfEnemies)
                {
                    Reset();
                    GameEnvironment.GameStateManager.SwitchTo("gameover");
                }

            }
           
         }
    }
}
