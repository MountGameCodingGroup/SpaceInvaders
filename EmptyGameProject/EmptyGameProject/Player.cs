﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyGameProject
{
    class Player : SpriteGameObject
    {
        private int speed;


        public Player(string assetName, int layer = 0, string id = "", int sheetIndex = 0)
       : base(assetName, layer, id, sheetIndex)
        {
            Reset();
        }

        public override void Reset()
        {
            base.Reset();

            speed = 400;
            Position = new Vector2(GameEnvironment.Screen.X / 2 - Width / 2, GameEnvironment.Screen.Y - Height * 4);
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            if (inputHelper.IsKeyDown(Keys.Left))
            {
                velocity.X = -speed;
            }
            else if (inputHelper.IsKeyDown(Keys.Right))
            {
                velocity.X = speed;
            }
            else
            {
                velocity.X = 0;
            }
        }
    }
}
