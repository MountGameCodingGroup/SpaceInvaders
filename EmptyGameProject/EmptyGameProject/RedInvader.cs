﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyGameProject
{
    class RedInvader : SpriteGameObject
    {
        private int speed;

        public RedInvader(string assetName, int layer = 0, string id = "", int sheetIndex = 0)
       : base(assetName, layer, id, sheetIndex)
        {
            Reset();
        }

        public override void Reset()
        {
            base.Reset();

            visible = true;
            speed = 30;
            Position = new Vector2(GameEnvironment.Screen.X / 2 - Width / 2, 0 + Height * 4);
            velocity.X = speed;
        }

        public override void Update(GameTime gameTime)
        {
            if (visible) {
                base.Update(gameTime);

                if (Position.X + Width >= GameEnvironment.Screen.X || Position.X <= 0)
                {
                    (parent as EnemyContainer).bounceEndOfFrame = true;
                }
            }
        }

        public void Bounce()
        {
            speed *= -1;
            velocity.X = speed;
            MoveDown();
        }

        public void HitBy(GameObject other)
        {
            visible = false;
            velocity.X = 0;
        }

        public void MoveDown()
        {
            position.Y += Height;

        }
    }
}
