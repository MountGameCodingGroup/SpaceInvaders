﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyGameProject
{
    class Bullet : SpriteGameObject
    {
        private int speed;

        public Bullet(string assetName, int layer = 0, string id = "", int sheetIndex = 0)
       : base(assetName, layer, id, sheetIndex)
        {
            speed = -500;       
            visible = false;
        }

        public override void Reset()
        {
            base.Reset();

            visible = false;
            velocity.Y = 0;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Position.Y + Height <= 0)
            {
                Reset();
            }
        }

        public void HitBy(GameObject other)
        {
            Reset();
        }

        public void Fire(Vector2 newStartPosition)
        {
            visible = true;
            Position = newStartPosition;
            velocity.Y = speed;
        }



    }
}
