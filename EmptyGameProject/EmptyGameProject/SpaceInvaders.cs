﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EmptyGameProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class SpaceInvaders : GameEnvironment
    {
        private Level level;
        private Gameover gameover;


        public SpaceInvaders() : base()
        {
            Content.RootDirectory = "Content";
            windowSize = new Point(800, 600);
            Screen = windowSize;
            ApplyResolutionSettings();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();

            level = new Level();
            GameStateManager.AddGameState("level", level);
            gameover = new Gameover();
            GameStateManager.AddGameState("gameover", gameover);

            GameStateManager.SwitchTo("level");

        }

    }
}
