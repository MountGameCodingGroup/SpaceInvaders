﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyGameProject
{
    class EnemyContainer : GameObjectList
    {
        public bool bounceEndOfFrame;

        public EnemyContainer(int layer = 0, string id = "") : base(layer, id)
        {

        }

        public override void Reset()
        {
            base.Reset();

            bounceEndOfFrame = false;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (bounceEndOfFrame)
            {
                bounceEndOfFrame = false;
                foreach (RedInvader invader in Children)
                {
                    invader.Bounce();
                }
            }

        }

    }
}
